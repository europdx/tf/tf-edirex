resource "openstack_compute_instance_v2" "spreg" {
  name            = "spreg"
  image_name      = "debian-10-x86_64"
  flavor_name     = "standard.medium"
  key_pair        = "pvyskocil"
  security_groups = ["default", "ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni"]

  block_device {
    uuid                  = "bbe44bf3-b504-4a63-94f0-e51a69d9b7ba"
    source_type           = "image"
    volume_size           = 100
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group = "g_spreg"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_spreg" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_spreg_a" {
  floating_ip = openstack_networking_floatingip_v2.fip_spreg.address
  instance_id = openstack_compute_instance_v2.spreg.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_spreg" {
  ip_address = openstack_networking_floatingip_v2.fip_spreg.address
}

# Set CNAME record
resource "dns_cname_record" "cname_spreg" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.spreg.name
  cname = data.dns_ptr_record_set.ptr_spreg.ptr
  ttl   = var.ttl
}

# PTR record as output
output "spreg_addr" {
  value = data.dns_ptr_record_set.ptr_spreg.ptr
}
