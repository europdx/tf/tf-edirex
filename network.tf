resource "openstack_networking_network_v2" "network_edirex_2" {
  name           = "edirex_2"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_edirex_2" {
  name       = "edirex_2_subnet"
  network_id = "${openstack_networking_network_v2.network_edirex_2.id}"
  cidr       = "192.168.8.0/24"
  ip_version = 4
}

resource "openstack_networking_router_v2" "router_edirex_2" {
  name                = "edirex_2_router"
  admin_state_up      = true
  external_network_id = var.public-cesnet-78-128-251-GROUP_id
}

resource "openstack_networking_router_interface_v2" "router_edirex_2_interface_1" {
  router_id = "${openstack_networking_router_v2.router_edirex_2.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_edirex_2.id}"
}