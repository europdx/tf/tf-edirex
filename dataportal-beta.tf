resource "openstack_compute_instance_v2" "dataportal_beta" {
  #  count           = 1
  name            = "dataportal-beta"
  image_name      = var.image_name
  flavor_name     = var.flavor_2cpu4ram
  key_pair        = var.key_pair
  power_state     = "shutoff"
  security_groups = ["default", "ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni"]

  block_device {
    uuid                  = var.image_uuid
    source_type           = "image"
    volume_size           = 100
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group        = "g_dataportal-beta"
    ansible_user = "ubuntu"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_dataportal_beta" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_dataportal_beta_a" {
  floating_ip = openstack_networking_floatingip_v2.fip_dataportal_beta.address
  instance_id = openstack_compute_instance_v2.dataportal_beta.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_dataportal_beta" {
  ip_address = openstack_networking_floatingip_v2.fip_dataportal_beta.address
}

# Set CNAME record
resource "dns_cname_record" "cname_dataportal_beta" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.dataportal_beta.name
  cname = data.dns_ptr_record_set.ptr_dataportal_beta.ptr
  ttl   = var.ttl
}
