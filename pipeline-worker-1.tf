resource "openstack_compute_instance_v2" "pipeline-worker-1" {
  #  count           = 1
  name            = "pipeline-worker-1"
  image_name      = var.image_name
  flavor_name     = var.flavor_16cpu64ram_hpc_ssd_ephem
  key_pair        = var.key_pair
  security_groups = ["default", "ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni"]

  block_device {
    uuid                  = var.image_uuid
    source_type           = "image"
    volume_size           = 1000
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group        = "g_pipeline"
    ansible_user = "ubuntu"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_pipeline-worker-1" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_pipeline-worker-1" {
  floating_ip = openstack_networking_floatingip_v2.fip_pipeline-worker-1.address
  instance_id = openstack_compute_instance_v2.pipeline-worker-1.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_pipeline-worker-1" {
  ip_address = openstack_networking_floatingip_v2.fip_pipeline-worker-1.address
}

# Set CNAME record
resource "dns_cname_record" "cname_pipeline-worker-1" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.pipeline-worker-1.name
  cname = data.dns_ptr_record_set.ptr_pipeline-worker-1.ptr
  ttl   = var.ttl
}