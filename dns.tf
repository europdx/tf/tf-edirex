# A Records

resource "dns_a_record_set" "vhio" {
  zone      = var.zone
  name      = "vhio"
  addresses = ["147.251.4.91"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "cruk" {
  zone      = var.zone
  name      = "cruk"
  addresses = ["147.251.4.92"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "curie" {
  zone      = var.zone
  name      = "curie"
  addresses = ["147.251.4.89"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "nki" {
  zone      = var.zone
  name      = "nki"
  addresses = ["147.251.4.90"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "runner" {
  zone      = var.zone
  name      = "runner"
  addresses = ["147.251.4.93"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "backup" {
  zone      = var.zone
  name      = "backup"
  addresses = ["147.251.4.96"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "kul" {
  zone      = var.zone
  name      = "kul"
  addresses = ["147.251.4.97"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "las" {
  zone      = var.zone
  name      = "las"
  addresses = ["147.251.4.99"]
  ttl       = var.ttl
}

resource "dns_a_record_set" "lasleuven" {
  zone      = var.zone
  name      = "lasleuven"
  addresses = ["147.251.4.100"]
  ttl       = var.ttl
}


# CNAME records

resource "dns_cname_record" "cbioportal" {
  zone  = var.zone
  name  = "cbioportal"
  cname = "ip-78-128-251-27.flt.cloud.muni.cz."
  ttl = var.ttl
}

resource "dns_cname_record" "cbioportal-tmp" {
  zone  = var.zone
  name  = "cbioportal-tmp"
  cname = "ip-78-128-251-27.flt.cloud.muni.cz."
  ttl = var.ttl
}

resource "dns_cname_record" "galaxy-sandbox-test" {
  zone  = var.zone
  name  = "galaxy-sandbox-test"
  cname = "ip-78-128-251-202.flt.cloud.muni.cz."
  ttl   = var.ttl
}

resource "dns_cname_record" "download" {
  zone  = var.zone
  name  = "download"
  cname = "ip-78-128-251-79.flt.cloud.muni.cz."
  ttl   = var.ttl
}

resource "dns_cname_record" "usegalaxy" {
  zone  = var.zone
  name  = "usegalaxy"
  cname = "ip-78-128-250-111.flt.cloud.muni.cz."
  ttl   = var.ttl
}
