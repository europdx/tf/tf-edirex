#variable "application_credential_id" {
#  description = "TODO"
#}

#variable "application_credential_secret" {
#  description = "TODO"
#}

variable "network_name" {
  default = "auto_allocated_network"
}

variable "network_edirex2" {
  default = "edirex2"
}

variable "network_edirex_2" {
  default = "edirex_2"
}

variable "pool" {
  default = "public-cesnet-78-128-251-GROUP"
}

variable "public-cesnet-78-128-251-GROUP_id" {
  default = "410e1b3a-1971-446b-b835-bf503917680d"
}

variable "key_pair" {
  default = "tf-edirex"
}

variable "key_pair_rpesa" {
  default = "rpesa"
}

variable "dns_key_secret" {
}

variable "zone" {
  default = "edirex.ics.muni.cz."
}

variable "ttl" {
  default = 300
}

#ubuntu-bionic-x86_64
variable "image_name" {
  default = "ubuntu-bionic-x86_64"
}

#ubuntu-bionic-x86_64
variable "image_uuid" {
  default = "e8d75fc1-ac32-4851-90b5-b4c925e9e6f8"
}

#ubuntu-focal-x86_64
variable "image_focal_name" {
  default = "ubuntu-focal-x86_64"
}

#ubuntu-focal-x86_64
variable "image_focal_uuid" {
  default = "e3cc6247-d581-4c83-81bd-efbf41dc5beb"
}

# uuid for ubuntu-focal-x86_64-2020-05-13
variable "image_uuid_focal_x86_64_2020_05_13" {
  default = "e3cc6247-d581-4c83-81bd-efbf41dc5beb"
}

variable "flavor_4cpu16ram" {
  default = "standard.xlarge"
}

variable "flavor_8cpu32ram" {
  default = "standard.xxlarge"
}

variable "flavor_4cpu8ram" {
  default = "standard.large"
}

variable "flavor_2cpu4ram" {
  default = "standard.medium"
}

variable "flavor_1cpu1ram" {
  default = "standard.tiny"
}

variable "flavor_16cpu64ram_hpc_ssd_ephem" {
  default = "hpc.16core-64ram-ssd-ephem"
}

variable "flavor_8cpu32ram_hpc_ssd_rcx_ephem" {
  default = "hpc.8core-32ram-ssd-rcx-ephem"
}

variable "flavor_hpc_4core_16ram_ssd_ephem" {
  default = "hpc.4core-16ram-ssd-ephem"
}