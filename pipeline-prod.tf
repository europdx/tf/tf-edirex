resource "openstack_compute_instance_v2" "pipeline-prod" {
  #  count           = 1
  name            = "pipeline-prod"
  image_name      = var.image_name
  flavor_name     = var.flavor_8cpu32ram
  key_pair        = var.key_pair
  security_groups = ["default", "ingress_pftp", "ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni", "ingress_galaxy"]

  block_device {
    uuid                  = var.image_uuid
    source_type           = "image"
    volume_size           = 1000
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group        = "g_pipeline"
    ansible_user = "ubuntu"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_pipeline-prod" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_pipeline-prod_a" {
  floating_ip = openstack_networking_floatingip_v2.fip_pipeline-prod.address
  instance_id = openstack_compute_instance_v2.pipeline-prod.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_pipeline-prod" {
  ip_address = openstack_networking_floatingip_v2.fip_pipeline-prod.address
}

# Set CNAME record
resource "dns_cname_record" "cname_pipeline-prod" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.pipeline-prod.name
  cname = data.dns_ptr_record_set.ptr_pipeline-prod.ptr
  ttl   = var.ttl
}