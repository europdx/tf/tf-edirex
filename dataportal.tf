resource "openstack_compute_instance_v2" "dataportal" {
  #  count           = 1
  name            = "dataportal"
  image_name      = var.image_name
  flavor_name     = var.flavor_4cpu8ram
  key_pair        = var.key_pair
  security_groups = ["default", "ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni"]

  block_device {
    uuid                  = var.image_uuid
    source_type           = "image"
    volume_size           = 100
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group        = "g_dataportal"
    ansible_user = "ubuntu"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_dataportal" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_dataportal_a" {
  floating_ip = openstack_networking_floatingip_v2.fip_dataportal.address
  instance_id = openstack_compute_instance_v2.dataportal.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_dataportal" {
  ip_address = openstack_networking_floatingip_v2.fip_dataportal.address
}

# Set CNAME record
resource "dns_cname_record" "cname_dataportal" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.dataportal.name
  cname = data.dns_ptr_record_set.ptr_dataportal.ptr
  ttl   = var.ttl
}
