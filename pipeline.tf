resource "openstack_compute_instance_v2" "pipeline" {
  #  count           = 1
  name            = "pipeline"
  image_name      = var.image_name
  flavor_name     = var.flavor_8cpu32ram
  key_pair        = var.key_pair
  security_groups = ["default", "ingress_pftp","ingress_ssh", "allow_ingress_http_https", "ingress_icmp", "allow_egress_ip4", "ingress_muni", "ingress_galaxy"]

  block_device {
    uuid                  = var.image_uuid
    source_type           = "image"
    volume_size           = 500
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    group        = "g_pipeline"
    ansible_user = "ubuntu"
  }

  network {
    name = var.network_name
  }
}

resource "openstack_networking_floatingip_v2" "fip_pipeline" {
  pool = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "fip_pipeline_a" {
  floating_ip = openstack_networking_floatingip_v2.fip_pipeline.address
  instance_id = openstack_compute_instance_v2.pipeline.id
}

# Get PTR record
data "dns_ptr_record_set" "ptr_pipeline" {
  ip_address = openstack_networking_floatingip_v2.fip_pipeline.address
}

# Set CNAME record
resource "dns_cname_record" "cname_pipeline" {
  zone  = var.zone
  name  = openstack_compute_instance_v2.pipeline.name
  cname = data.dns_ptr_record_set.ptr_pipeline.ptr
  ttl   = var.ttl
}