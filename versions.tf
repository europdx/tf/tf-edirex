terraform {
  required_providers {
    dns = {
      source = "hashicorp/dns"
    }
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
  required_version = ">= 0.13"
}
