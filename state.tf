terraform {
  backend "swift" {
    container         = "edirex_terraform_state"
    archive_container = "edirex_terraform-state-archive"
  }
}
