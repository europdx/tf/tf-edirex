resource "openstack_networking_secgroup_v2" "all_open" {
  name        = "all_open"
  description = "Any traffic allowed."
}

resource "openstack_networking_secgroup_v2" "Outgoing_traffic" {
  name        = "outgoing_traffic"
  description = "Outgoing_traffic allowed."
}

resource "openstack_networking_secgroup_v2" "ingress_ssh" {
  name        = "ingress_ssh"
  description = "Ingress SSH"
}

resource "openstack_networking_secgroup_v2" "ingress_muni" {
  name        = "ingress_muni"
  description = "Ingress from Muni"
}

resource "openstack_networking_secgroup_v2" "ingress_pftp" {
  name        = "ingress_pftp"
  description = "Ingress to passive ftp ports"
}


resource "openstack_networking_secgroup_v2" "allow_barn" {
  name        = "allow_barn"
  description = "allow_barn"
}

resource "openstack_networking_secgroup_v2" "allow_rancher" {
  name        = "allow_rancher"
  description = "allow_rancher"
}

resource "openstack_networking_secgroup_v2" "allow_ingress_http_https" {
  name        = "allow_ingress_http_https"
  description = "Allow_ingress_http_https from anywhere"
}

resource "openstack_networking_secgroup_v2" "allow_egress_ip4" {
  name        = "allow_egress_ip4"
  description = "Allow_egress_ip4"
}

resource "openstack_networking_secgroup_v2" "ingress_icmp" {
  name        = "ingress_icmp"
  description = "Ingress ICMP"
}

resource "openstack_networking_secgroup_v2" "ingress_galaxy" {
  name        = "ingress_galaxy"
  description = "Open Galaxy ports 21, 8080, 9007 and 9009"
}

resource "openstack_networking_secgroup_v2" "allow_ingress_k3s" {
  name        = "allow_ingress_k3s"
  description = "Open K3s ports 6443"
}

resource "openstack_networking_secgroup_v2" "allow_ingress_32404" {
  name        = "allow_ingress_32404"
  description = "Open port 32404"
}


###############################################################################
resource "openstack_networking_secgroup_rule_v2" "allow_muni_ingress" {
  direction = "ingress"
  ethertype = "IPv4"
  #protocol          = "tcp"
  #port_range_min    = 22
  #port_range_max    = 22
  remote_ip_prefix = "147.251.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.ingress_muni.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_pftp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 49101
  port_range_max    = 49150
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_pftp.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_cesnet_cloud_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "78.128.251.0/24"
  security_group_id = openstack_networking_secgroup_v2.ingress_muni.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_muni_ingress_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "147.251.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.ingress_muni.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_ingress_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_icmp.id
}


resource "openstack_networking_secgroup_rule_v2" "ssh_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_ssh.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_barn_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "147.251.6.179/32"
  security_group_id = openstack_networking_secgroup_v2.allow_barn.id
}

resource "openstack_networking_secgroup_rule_v2" "allow_rancher_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "78.128.251.185/32"
  security_group_id = openstack_networking_secgroup_v2.allow_rancher.id
}

resource "openstack_networking_secgroup_rule_v2" "http_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ingress_http_https.id
}

resource "openstack_networking_secgroup_rule_v2" "https_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ingress_http_https.id
}

resource "openstack_networking_secgroup_rule_v2" "egress_rule" {
  direction = "egress"
  ethertype = "IPv4"
  protocol  = "tcp"

  #  port_range_min    = 443
  #  port_range_max    = 443
  remote_ip_prefix = "0.0.0.0/0"

  security_group_id = openstack_networking_secgroup_v2.allow_egress_ip4.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_8080" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_9007" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9007
  port_range_max    = 9007
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_udp_8080" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_udp_9007" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 9007
  port_range_max    = 9007
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_9009" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9009
  port_range_max    = 9009
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_20" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 20
  port_range_max    = 20
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_21" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 21
  port_range_max    = 21
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ingress_galaxy.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_6443" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ingress_k3s.id
}

resource "openstack_networking_secgroup_rule_v2" "port_tcp_32404" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 32404
  port_range_max    = 32404
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.allow_ingress_32404.id
}
